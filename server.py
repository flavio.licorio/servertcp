import socket
import threading
import requests

from datetime import datetime

# data = b'xx\x1f\x12\x12\x08\x1c\x15\x10\x06\xc7\x02_H\x10\x05b\xac\xeb\x00\x18\x00\x02\xd4\n\x04\xbe\x00\x04\xef\x00\t\x9b\xd2\r\n'
# data = b'xx\x1f\x12\x12\x08\x1d\x16\x078\xc6\x02^+P\x05e\xb5\xeb\x00\x18\x00\x02\xd4\n\x04\xbe\x00\x055\x00\x17y@\r\n'
#data = b'xx\n\x13D\x06\x04\x00\x00\x00)\xa5&\r\n'
#bdata = b'xx\x1f\x12\x12\x08\x1d\x159\x00\xc8\x02^*\xb1\x05e\xb5\xa3\x00\x18\x00\x02\xd4\n\x04\xbe\x00\x055\x00\xa8n\xcc\r\nxx\x1f\x12\x12\x08\x1d\x159\n\xc9\x02^*\xb1\x05e\xb5\xa5\x00\x18\x00\x02\xd4\n\x04\xbe\x00\x055\x00\xa9\xad\xfa\r\nxx\x1f\x12\x12\x08\x1d\x159\x14\xc9\x02^*\xb4\x05e\xb5\xa9\x00\x18\x00\x02\xd4\n\x04\xbe\x00\x055\x00\xaa\x1eb\r\nxx\n\x13D\x06\x04\x00\x00\x00\xab\x02<\r\nxx\x1f\x12\x12\x08\x1d\x159\x1e\xc9\x02^*\xb6\x05e\xb5\xac\x00\x18\x00\x02\xd4\n\x04\xbe\x00\x055\x00\xaczW\r\nxx\x1f\x12\x12\x08\x1d\x159(\xc9\x02^*\xba\x05e\xb5\xac\x00\x18\x00\x02\xd4\n\x04\xbe\x00\x055\x00\xad/\x82\r\nxx\x1f\x12\x12\x08\x1d\x1592\xc9\x02^*\xc3\x05e\xb5\xb1\x00\x18\x00\x02\xd4\n\x04\xbe\x00\x055\x00\xae\xd6N\r\nxx\x1f\x12\x12\x08\x1d\x15:\x00\xc8\x02^*\xc0\x05e\xb5\xaf\x00\x18\x00\x02\xd4\n\x04\xbe\x00\x055\x00\xaf\xc2\r\r\nxx\x1f\x12\x12\x08\x1d\x15:\n\xc8\x02^*\xba\x05e\xb5\xb1\x00\x18\x00\x02\xd4\n\x04\xbe\x00\x055\x00\xb0\xbf2\r\nxx\x1f\x12\x12\x08\x1d\x15:\x14\xc8\x02^*\xba\x05e\xb5\xbb\x00\x18\x00\x02\xd4\n\x04\xbe\x00\x055\x00\xb1"\x9d\r\nxx\x1f\x12\x12\x08\x1d\x15:\x1e\xc8\x02^*\xc3\x05e\xb5\xbb\x00\x18\x00\x02\xd4\n\x04\xbe\x00\x055\x00\xb2\xb3\xae\r\nxx\x1f\x12\x12\x08\x1d\x15:(\xc8\x02^*\xcc\x05e\xb5\xb1\x00\x18\x00\x02\xd4\n\x04\xbe\x00\x055\x00\xb3\xf5\xf4\r\nxx\x1f\x12\x12\x08\x1d\x15:2\xc9\x02^*\xd5\x05e\xb5\xa3\x00\x18\x00\x02\xd4\n\x04\xbe\x00\x055\x00\xb4[\xa5\r\nxx\x1f\x12\x12\x08\x1d\x15;\x00\xc9\x02^*\xd1\x05e\xb5\x8e\x00\x18\x00\x02\xd4\n\x04\xbe\x00\x055\x00\xb5Z\xf2\r\nxx\x1f\x12\x12\x08\x1d\x15;\n\xc9\x02^*\xcc\x05e\xb5s\x00\x18\x00\x02\xd4\n\x04\xbe\x00\x055\x00\xb6\xc3\x87\r\n'



def to_hex(data):
    return list(map( lambda it: hex(ord(it))[2:].zfill(2), data))

def to_numbers(data):
    return list(map( lambda it: ord(it), data))


def read_number(data,size=4,order=1):
    to_parse = data[:size]
    to_parse = to_parse[::order]
    return sum(ord(d) << (8 * (size - i) ) for d,i in zip(data,range(1,size+1)))


def read_date(data):
    year,month,day,hour,minute,second = list(map(ord,data))
    return datetime(year+2000,month,day,hour,minute,second)

def read(data):
    package = list(map(ord,data))
    lista = []
    for i in package:
        lista.append(hex(i)[2:].zfill(2))
    year = int(lista[4],16)+2000
    month = int(lista[5],16)
    day = int(lista[6],16)
    # date = str(day)+"/"+str(month)+"/"+str(year)
    hour = int(lista[7],16)
    minute = int(lista[8],16)
    second = int(lista[9],16)
    # time = str(hour)+":"+str(minute)+":"+str(second)
    atual = datetime(year,month,day,hour,minute,second)
    lat = lista[11]+lista[12]+lista[13]+lista[14]
    lon = lista[15]+lista[16]+lista[17]+lista[18]
    lat = float(int(lat,16))/30000/60
    lon = float(int(lon,16))/30000/60
    vel = int(lista[19])
    temp = bin(int(lista[20],16))[2:].zfill(8)
    course = bin(int(lista[21],16))[2:].zfill(8)
    west = temp[3]
    north = temp[2]
    course = int(str(temp[0:2])+str(course),2)
    mcc = int((lista[22]+lista[23]),16)
    mnc = int(lista[24],16)
    lac = int((lista[25]+lista[26]),16)
    cell = int((lista[27]+lista[28]+lista[29]),16)
    idcell = int(str(mcc)+str(mnc)+str(lac)+str(cell))
    if west == "1":
        lon = lon*(-1)
    if north == "0":
        lat = lat*(-1)
    resul = {
        "versao": "0.0",
        "datetime": atual,
        "lat": lat,
        "lon": lon,
        "velocidade": vel,
        "direcao": course,
        "id": idcell
    }

    return resul

HOST = "0.0.0.0"
PORT = 8000

url = "https://api-geocar.herokuapp.com/dispositivos/"

def conectado(con, cliente):
    print('Estamos conectados amigo!', cliente)
    while True:
        msg = con.recv(1024)
        if not msg:break
        print(cliente,msg)
        try:
            result = read(msg)
            requests.post(url,data=result)
            print(result)
        except:
            print("deu nao")
            pass
        con.send(msg)
    print('Que pena que Acaboooou!', cliente)
    con.close()

tcp = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
tcp.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
orig = (HOST,PORT)
tcp.bind(orig)
tcp.listen(1)

while True:
    con, cliente = tcp.accept()
    threading._start_new_thread(conectado, tuple([con, cliente]))

tcp.close()
